# Configure the OpenStack Provider
provider "openstack" {
  user_name   = ""
  password    = ""
  auth_url    = "https://vprdvio.foodstuffs.co.nz:5000/v3"
  region      = "nova"
  tenant_id  = "1924f432d5a24e6d944bf02658d9e526"
  tenant_name = "fsni-cd-services"
  domain_name = "default"
  insecure="true"
}
