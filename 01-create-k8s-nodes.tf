resource "openstack_compute_secgroup_v2" "fsni-Ssg-k8s-cluster" {
  name        = "fsni-sg-k8s-cluster"
  description = "Security Group for K8s Cluster on Openstack"

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 80
    to_port     = 80
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  # rule {
  #   from_port = 8
  #   to_port = 0
  #   ip_protocol = "icmp"
  #   cidr = "0.0.0.0/0"
  # }
}

# resource "openstack_compute_instance_v2" "k8s-master" {
#   name = "k8s-master"
#   image_name = "${var.fsni_image_name}"
#   flavor_name = "${var.fsni_image_flavor}"
#   key_pair = "${var.fsni_key_pair}"
#   #security_groups = ["${split(",", var.fsni_security_groups)}"]
#   security_groups = ["${openstack_compute_secgroup_v2.fsni-Ssg-k8s-cluster.id}"]
#   availability_zone = "${var.fsni_aza_region}"
#   network {
#     name = "${var.fsni_aza_k8s_pool}"
#   }
  
# }

#  resource "openstack_compute_instance_v2" "k8s-aza-node" {
#    count = "${var.fsni_k8s_nodes/2}"
#    name = "k8s-aza-node-${count.index}"
#    image_name = "${var.fsni_image_name}"
#    flavor_name = "${var.fsni_image_flavor}"
#    key_pair = "${var.fsni_key_pair}"
#    #security_groups = ["${split(",", var.fsni_security_groups)}"]
#    security_groups = ["${openstack_compute_secgroup_v2.fsni-Ssg-k8s-cluster.id}"]
#    availability_zone = "${var.fsni_aza_region}"
#    network {
#      name = "${var.fsni_aza_k8s_pool}"
#    }
#  }

#  resource "openstack_compute_instance_v2" "k8s-azb-node" {
#    count = "${var.fsni_k8s_nodes/2}"
#    name = "k8s-azb-node-${count.index}"
#    image_name = "${var.fsni_image_name}"
#    flavor_name = "${var.fsni_image_flavor}"
#    key_pair = "${var.fsni_key_pair}"
#    #security_groups = ["${split(",", var.fsni_security_groups)}"]
#    security_groups = ["${openstack_compute_secgroup_v2.fsni-Ssg-k8s-cluster.id}"]
#    availability_zone = "${var.fsni_azb_region}"
#    network {
#      name = "${var.fsni_azb_k8s_pool}"
#    }
#  }