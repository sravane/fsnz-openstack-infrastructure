##Setup needed variables
variable "fsni_azs" {}
variable "fsni_aza_router" {}
variable "fsni_azb_router" {}
variable "fsni_project" {}
variable "fsni_aza_cidr" {}
variable "fsni_azb_cidr" {}
variable "fsni_aza_region" {}
variable "fsni_azb_region" {}
variable "fsni_aza_ext_network" {}
variable "fsni_azb_ext_network" {}
variable "fsni_key_pair" {}
variable "fsni_k8s_nodes" {}
variable "fsni_aza_k8s_pool" {}
variable "fsni_azb_k8s_pool" {}
variable "fsni_image_name" {}
variable "fsni_image_flavor" {}
variable "fsni_security_groups" {}

#data “terraform_remote_state” “state” {
#  backend = “s3”
#  config {
#    bucket = “fsnz-terraform-state”
#    key    = “playpen-network/openstack.tfstate”
#    region = “ap-southeast-2"
#  }
#}


variable "fsniazs" {
  description = "FSNI Openstack Availability Zones"
  type = "list"
  default = ["aza", "azb"]
}

variable "fsniazscidr" {
  default = {
      aza="10.8.156.0/22",
      azb="10.9.156.0/22"
  }}

  

variable "fsniazexternalgateway" {
  default = {
      azb="3389dcc1-ecbb-4308-a110-68925b992caf"
      aza="addc2d2f-f582-42e4-b61f-08aca279a5a3"
  }}

variable "fsninetworks" {
  description = "FSNI Openstack Networks"
  type = "list"
  default = ["BE-LB-1","DNS-1","FE-LB-1","FE-LB-2","FE-Services-API-1","Kubernetes-LB-1","Servers-1"]
}

variable "fsnisubnetnewbits" {
  description = "FSNI Openstack Subnets"
  default = {
      BE-LB-1="5" #forms the /27 block
      DNS-1="6" # forms the /28 block
      FE-LB-1="6"
      FE-LB-2="6"
      FE-Services-API-1="6"
      Kubernetes-LB-1="3"
      Servers-1="5"
}
}

variable "fsnisubnetnetnumber" {
  description = "FSNI Openstack Subnets"
  default = {
      BE-LB-1="5" # forms the .160 in the 160/27 block
      DNS-1="8" # forms the 128 in .128/28 block
      FE-LB-1="0"
      FE-LB-2="1"
      FE-Services-API-1="2"
      Kubernetes-LB-1="2"
      Servers-1="6"
}
}
#  #routes and static routes to be done manually for now until availability_zone_hints issue is sorted. Possibly a bug
#   resource "openstack_networking_router_v2" "fsnirouters" {
#     count = "${length(var.fsniazs)}"
#     name  = "${replace("${var.fsni_project}","fsni-","")}-${element(var.fsniazs, count.index)}"
#     external_gateway = "${lookup(var.fsniazexternalgateway,"${element(var.fsniazs, count.index)}")}"
#     admin_state_up = "true"
#     distributed = "true"
#     enable_snat = "false"
#     value_specs = {
#       availability_zone_hints = "$${FSNI_AZA}"
#    }
#    #external gateway must be passed on as a variable
# }

 resource "openstack_networking_network_v2" "openstack-networks" {
   admin_state_up = "true"
   count = "${length(var.fsninetworks) * length(var.fsniazs)}"
   name = "Subnet-${replace("${var.fsni_project}","fsni-cd-","")}-${element(var.fsninetworks,count.index)}${replace("${element(var.fsniazs, count.index)}","az","")}API"
 }
  resource "openstack_networking_subnet_v2" "openstack-subnet" {
   count = "${length(var.fsninetworks) * length(var.fsniazs)}"
   name            = "Foodstuffs-Services-Subnet${element(var.fsninetworks,count.index)}${replace("${element(var.fsniazs, count.index)}","az","")}API"
   network_id      = "${element(openstack_networking_network_v2.openstack-networks.*.id, count.index)}"
   cidr              = "${cidrsubnet("${lookup(var.fsniazscidr,"${element(var.fsniazs, count.index)}")}","${lookup(var.fsnisubnetnewbits,"${element(var.fsninetworks, count.index)}")}","${lookup(var.fsnisubnetnetnumber,"${element(var.fsninetworks, count.index)}")}")}"
   ip_version      = 4
   dns_nameservers = ["10.5.0.151", "10.6.0.151"]
 }

resource "openstack_networking_router_interface_v2" "openstack-router-interfaces" {
  count = "${length(var.fsninetworks)* 2}"
  router_id = "${substr("${element(openstack_networking_subnet_v2.openstack-subnet.*.name, count.index)}",-4,1) == "a" ? var.fsni_aza_router : var.fsni_azb_router}"
  #router_id = "${length(element(element(openstack_networking_subnet_v2.openstack-subnet.*.name, count.index))}"
  subnet_id = "${element(openstack_networking_subnet_v2.openstack-subnet.*.id, count.index)}"
}

 